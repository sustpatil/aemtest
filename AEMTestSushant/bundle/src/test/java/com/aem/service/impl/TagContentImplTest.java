package com.aem.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

@RunWith(MockitoJUnitRunner.class)
public class TagContentImplTest {
	
	private static final String TAGID_1 = "Article:News";
	private static final String TAGID_2 = "Article:Events";
	private static final String TAGID_3 = "Asset:Image";
	private static final String[] SEARCH_TAGS = {TAGID_1, TAGID_2, TAGID_3};
	
	
	private ResourceResolverFactory resolverFactory = mock(ResourceResolverFactory.class);
	
	@Mock 
	private ResourceResolver resourceResolver;
	
	@Mock
	private TagManager tagmanager;
	
	@Mock
	private Tag tag;
	
	private TagContentImpl tagContentImpl;
	
	@Before
	public void setUp()throws Exception{
        MockitoAnnotations.initMocks(this);
	    when(resolverFactory.getAdministrativeResourceResolver(null)).thenReturn(resourceResolver);
		//doReturn(resourceResolver).when(resolverFactory).getAdministrativeResourceResolver(null);
		doReturn(tagmanager).when(resourceResolver).adaptTo(TagManager.class);
		doReturn(tag).when(tagmanager).resolve(eq(TAGID_1));
		tagContentImpl = new TagContentImpl();
		//tagContentImpl.activate(null);
	}
	
	@Test
	public void testgetPageListofTags(){
		//tagContentImpl.getPageListofTags(SEARCH_TAGS, false);
	}
}

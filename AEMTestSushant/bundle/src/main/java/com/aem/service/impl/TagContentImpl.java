package com.aem.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.ComponentContext;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.service.TagContent;

/***
 * @author sushant 
 * Find AEM Pages from�/content� that contain a Tag or all the Tags passed as parameter and have been modified the last five days.
 *
 */

@Component(
		label="Content PageList of Tag", description="find list of content page of given tags",
		immediate=true)
@Service( value = TagContent.class)
public class TagContentImpl implements TagContent{

	private static final Logger LOG = LoggerFactory.getLogger(TagContentImpl.class);
	
	private static final String PROPERTY_LASTMODIFIED = "jcr:lastModified";
	private static final int NUMBER_OF_DAYS_BEFORE = 5;
	
	private TagManager tagManager;
	
	@Activate
	protected void activate(ComponentContext ctx){
	}

	/*
	 * Get page list of given tag
	 * @see com.aem.service.TagContent#getPageListofTags(java.lang.String[], ResourceResolver resolver)
	 */
	public Map<String,List<String>> getPageListofTags(String[] tags, ResourceResolver resolver) {
		List<Tag> tagList = getTags(tags, resolver);
		Map<String,List<String>> pageListMap = new HashMap<String,List<String>>();
		for(Tag tag: tagList){
			List<String> pageList = getTagPageList(tag);
			pageListMap.put(tag.getName(), pageList);
		}
		return pageListMap;
	}
	
	/*
	 * Get Page List when page contain all search tags
	 * @see com.aem.service.TagContent#getPageListofAllTag(java.lang.String[], ResourceResolver resolver)
	 */
	public List<String> getPageListofAllTag(String[] tags, ResourceResolver resolver) {
		List<Tag> tagList = getTags(tags, resolver);
		List<String> alltagcontentList = new ArrayList<String>();
		int index=0;
		for (Tag tag: tagList) {
			List<String> pageList = getTagPageList(tag);
			if (index == 0) {
				alltagcontentList.addAll(pageList);
			}
			else {
				alltagcontentList.retainAll(pageList);
			}
			index ++;
		}
		return alltagcontentList;
	}

	
	/*
	 * 
	 */
	private List<String> getTagPageList(Tag tag){
		List<String> pageList = new ArrayList<String>();
		Iterator<Resource> itreator = tag.find();
		while (itreator.hasNext()) {
			Resource resource = itreator.next();
			pageList.add(resource.getPath());
		}
		
		return pageList;
	}
	
	private List<Tag> getTags(String[] tags, ResourceResolver resolver) {
		List<Tag> tagList = null;
		if (tags != null) {
			tagList = new ArrayList<Tag>();
			for (String tagid : tags) {
				tagManager = resolver.adaptTo(TagManager.class);
				Tag tag = tagManager.resolve(tagid); 
				if (isTagModified(tag)) {
					tagList.add(tag);
				}
				
			}
		}
		else{
			LOG.info("Search tags are null. Please provide tags");
		}
		return tagList;
	}
	
	private boolean isTagModified(Tag tag){
		 Node node = tag.adaptTo(Node.class);
		 boolean islastModified = false;
		try {
			Property prop = node.getProperty(PROPERTY_LASTMODIFIED);
			if(prop != null){
				Calendar lastmodifiedDate = prop.getDate();
				Calendar currentDate = Calendar.getInstance();
				currentDate.add(Calendar.DATE, -NUMBER_OF_DAYS_BEFORE);
				islastModified = lastmodifiedDate.after(currentDate);
			}
			

		} catch (PathNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return islastModified;
	}
	
}

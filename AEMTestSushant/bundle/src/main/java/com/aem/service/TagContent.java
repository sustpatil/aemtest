package com.aem.service;

import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.ResourceResolver;

public interface TagContent {
	
	public Map<String,List<String>> getPageListofTags(String[] tags, ResourceResolver resolver);
	
	public List<String> getPageListofAllTag(String[] tags, ResourceResolver resolver);

}

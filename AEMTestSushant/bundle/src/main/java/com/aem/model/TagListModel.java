package com.aem.model;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;

import com.aem.service.TagContent;

/**
 * Sling model for Tag Linked List component
 * @author sushant
 *
 */
@Model(adaptables = Resource.class)
public class TagListModel {

	@Inject
	private String[] searchtag;
	
	@Inject @Default(booleanValues = false)
	private boolean includealltag;
	
	@Inject
	TagContent tagcontent;
	
	@Inject
	private ResourceResolver resolver;
 
	
	private Map<String,List<String>> tagcontenttList;
	
	private List<String> alltagcontentList;

	public String[] getSearchtag() {
		return searchtag;
	}

	public boolean isIncludealltag() {
		return includealltag;
	}
	
	/*get list of Page where given tag used
	 * 
	 */
	public Map<String, List<String>> getTagcontenttList() {
		tagcontenttList = tagcontent.getPageListofTags(searchtag, resolver);
		return tagcontenttList;
	}
	/*
	 * get list of Page where all tags used
	 */
	public List<String> getAlltagcontentList() {
		alltagcontentList = tagcontent.getPageListofAllTag(searchtag, resolver);
		return alltagcontentList;
	}
	
		
}
